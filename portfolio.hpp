#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__

#include "asset.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>



using namespace Eigen;




class Portfolio {
public:
  int num;
  VectorXd w;
  MatrixXd correlation;
  std::vector<Asset> a;
  double sd;
  double ror;
public:
  Portfolio(){}
  void calculateROR() {
    double r = 0;
    for (int i = 0; i <num; i++){
      r = r + w(i) * a[i].ror;
    }
    ror = r;
  }
  void calculateSD() {
    double s = 0;
    for(int i = 0; i < num; i++) {
      for(int j = 0; j <num; j++) {
	  s = s + w[i]* w[j] * correlation(i,j) * a[i].sd *a[j].sd;
	}
      }

    sd = std::sqrt(s);
  }
  
  ~Portfolio() {}
  
};
#endif
