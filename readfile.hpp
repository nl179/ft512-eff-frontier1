#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

bool comparedoubles(double x, double y);
Asset parseUniverse(std::string &line);
std::vector<std::string> parseCorrelation(std::string &line);
std::vector<Asset> readUniverse(char * file, int &num);
MatrixXd readCorrelation(char * file, int num);
double UnrestrictedCase(Portfolio &p, double &ror);
double RestrictedCase(Portfolio &p, double &ror);
#endif




