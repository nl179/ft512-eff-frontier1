#include "asset.hpp"
#include "readfile.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>
using namespace Eigen;
int main(int argc, char ** argv) {
  if(argc != 3 && argc != 4) {
    fprintf(stderr, "invalid input file\n");
    return EXIT_FAILURE;
  }
  // read universe.csv
  std::vector<Asset> asset;
  int num = 0; //number of asset
  MatrixXd correlation;
  if(argc == 4 && strlen(argv[1]) != 2) {
    fprintf(stderr, "invalid option\n");
    return EXIT_FAILURE;
  }
  bool x = false;
  Portfolio p;
  int c;
  std::cout<<"ROR,volatility"<<std::endl;
  while ((c = getopt(argc, argv, "r")) != -1) {
    switch(c) {
    case 'r':
      x = true;
      break;
    case '?':
      fprintf(stderr, "option is wrong\n");
      return EXIT_FAILURE;
    }
  }
  std::cout.setf(std::ios::fixed); 
  if(x == false) { // no -r
    asset = readUniverse(argv[1], num);
    correlation = readCorrelation(argv[2], num);
    p.correlation = correlation;
    p.a = asset;
    p.num = num;
    for(double i = 0.01; i <= 0.265; i += 0.01) {
      double minVol = UnrestrictedCase(p,i);
      std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<minVol*100<<"%"<<std::endl;
    }

  }
    /* code */
    else {//with r
      asset = readUniverse(argv[2], num);
      correlation = readCorrelation(argv[3], num);
      p.correlation = correlation;
      p.a = asset;
      p.num = num;
      for(double i = 0.01; i <= 0.265; i += 0.01) {
      double minVol = RestrictedCase(p,i);
        std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<minVol*100<<"%"<<std::endl;
    }

    }


  /*   p.correlation = correlation;
    p.a = asset;
    p.num = num;
    //    std::cout<<"number of assets is"<<p.num;
    std::cout.setf(std::ios::fixed);
    if(x == false) {
    for(double i = 0.01; i <= 0.26; i += 0.01) {
      double minVol = UnrestrictedCase(p,i);
        std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<minVol*100<<"%"<<std::endl;
    }
    }
    else{
      for(double i = 0.01; i <= 0.26; i += 0.01) {
      double minVol = RestrictedCase(p,i);
        std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<minVol*100<<"%"<<std::endl;
    }
    }
  */
    std::cout.unsetf(std::ios::fixed);
    return EXIT_SUCCESS;
    
  }
  
     
    
 
    
    
    

