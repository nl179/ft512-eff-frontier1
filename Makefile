CCFLAGS=--std=gnu++98 -pedantic -Wall -Werror -ggdb3
efficient_frontier:readfile.o main.o
	g++ -o efficient_frontier $(CCFLAGS) readfile.o main.o
parse.o: readfile.cpp readfile.hpp asset.hpp portfolio.hpp
	g++ -c $(CCFLAGS) -o readfile.cpp readfile.hpp
main.o: main.cpp asset.hpp readfile.hpp portfolio.hpp
	g++ -c $(CCFLAGS) -o main.o main.cpp

clean:
	rm -f *.o  *~ efficient_frontier
