#include "asset.hpp"
#include "portfolio.hpp"
#include "readfile.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

bool comparedoubles(double x, double y) {
  double epsilon = 0.0000001f;
  if(fabs(x - y) < epsilon) {
    return true; 
  }//they are same
  return false; //they are not same
}



Asset parseUniverse(std::string &line) {
  Asset a;
  std::istringstream ss(line);
  char delim = ',';
  std::vector<std::string> ret;
  std::string tmp;
  while(getline(ss, tmp, delim)){
    ret.push_back(tmp);
  }
  if(ret.size() != 3) {
    perror("invalid format for the line");
    exit(EXIT_FAILURE);
  }
  
  a.name = ret[0];
  if(atof(ret[1].c_str())==0.0 || atof(ret[2].c_str())==0.0){
    perror("return and sd are non-numeric values");//handle non-numeric value
    exit(EXIT_FAILURE);
  }
  a.ror = atof(ret[1].c_str());
  a.sd = atof(ret[2].c_str());
  //    ret.pop_back();//delect the newline in the last element of the vector
  return a;
}
std::vector<std::string> parseCorrelation(std::string &line) {
  std::istringstream ss(line);
  char delim = ',';
  std::vector<std::string> ret;
  std::string tmp;
  while(getline(ss, tmp, delim)){
    ret.push_back(tmp);
  }
  return ret;
}

std::vector<Asset> readUniverse(char * file, int &num) {
  std::vector<Asset> v;
  std::ifstream f1(file);
  if(!f1.is_open()) {
    perror("can not open universe file\n");
    exit( EXIT_FAILURE);
  }
  std::string line;
  while(std::getline(f1,line)) 
    { num++;
      Asset temp = parseUniverse(line);
      v.push_back(temp);
      
      
      
    }
  if(num == 0) {
    perror("universe file is empty\n");//handle non-numeric value
    exit(EXIT_FAILURE);
  }
  return v; 
}

MatrixXd readCorrelation(char * file, int num) {
  int i = 0;
  MatrixXd correlation(num, num);
  std::ifstream f2(file);
  if(!f2.is_open()) {
    perror("can not open correlation\n");
    exit(EXIT_FAILURE);
  }
  std::string line;
  while(std::getline(f2,line)) {
    std::vector<std::string> temp = parseCorrelation(line);
    if(temp.size()!= (size_t)num) {
      perror("the asset number of two file does not match\n");
	exit(EXIT_FAILURE);
    }
    for(int j = 0; j < num; j++) {
      if(atof(temp[j].c_str()) == 0.0){
	perror("correlation matrix has nondigit content\n");
	  exit(EXIT_FAILURE);
      }
      correlation(i, j) = atof(temp[j].c_str());
    }
    i++;
  }
  if(i == 0){
    perror("correlation is empty file\n");
      exit(EXIT_FAILURE);
  }
  if(i != num) {
    perror("lines of correlation files does not match the number of asstes\n");
      exit(EXIT_FAILURE);
  }
  //check if the correlation is correct
  //1.check (i,j)  equals to (j,i)
  //2.check (i, i) equals to 1
  double one = 1.000;
  for(int i = 0; i < num; i++) {
    if(!comparedoubles(correlation(i,i), one)) {
	perror("correlation(i,i) != 1\n");
	exit(EXIT_FAILURE);
    }
    for(int j = 0; j <num; j++) {
      if(fabs(correlation(i,j) )>1.0001) {
	  perror("correlation greater than one\n");
	    exit(EXIT_FAILURE);
	}
      if(!comparedoubles(correlation(i,j), correlation(j,i))) {
	perror("correlation(i,j) != correlation(j,i)\n");
	  exit(EXIT_FAILURE);
      }
    }
  }
  return correlation;
  
}
//calculate Unrestricted case
double UnrestrictedCase(Portfolio &p, double &ror) {
  int n = p.num;
  MatrixXd x = MatrixXd::Ones(1,n);
  MatrixXd y(1,n);
  for(int i=0;i<n;i++){
    y(i) = p.a[i].ror;
  }
  MatrixXd z(2,n);
  z<<x,y;
  MatrixXd e = MatrixXd::Zero(n,1);
  MatrixXd f(2,1);
  f<<1,ror;
  MatrixXd g(n+2,1);
  g<<e,f;
  MatrixXd Covariance(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Covariance(i,j) = p.a[i].sd * p.correlation(i,j) * p.a[j].sd;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd h(n+2,n+2);
  h<<Covariance,z.transpose(),z,O;
  VectorXd v(n+2,1);
  v = h.fullPivHouseholderQr().solve(g);
  p.w = v.head(n);
  p.calculateSD();
  return p.sd;
}
double RestrictedCase(Portfolio &p, double &ror) {
  int n = p.num;
  MatrixXd Aa = MatrixXd::Ones(1,n);
  MatrixXd Ab(1,n);
  for(int i=0;i<n;i++){
    Ab(i) = p.a[i].ror;
  }
  MatrixXd A(2,n);
  A<<Aa,Ab;
  MatrixXd ba = MatrixXd::Zero(n,1);
  MatrixXd bb(2,1);
  bb<<1,ror;
  MatrixXd B(n+2,1);
  B<<ba,bb;
  MatrixXd Covariance(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Covariance(i,j) = p.a[i].sd * p.correlation(i,j) * p.a[j].sd;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);  
  MatrixXd L(n+2,n+2);
  L<<Covariance,A.transpose(),A,O;
  VectorXd N(n+2,1);
  N = L.fullPivHouseholderQr().solve(B);
  MatrixXd AA = A;
  MatrixXd BB = B;
  VectorXd NN = N;
  for(int i=1;;i++){
    int flag = 1;
    MatrixXd C;
    int k=0;
    for(int j=0;j<n;j++){
      if(NN(j) < 0){
        MatrixXd Y = MatrixXd::Zero(n,1);
        Y(j,0) = 1;
        MatrixXd temp = C;
        C.resize(n,k+1);
        if(k==0){
          C<<Y;
        }
        else{
          C<<temp,Y;
        }
        flag = 0;
        k++;
      }
    }
    if(flag == 1){
      break;
    }
    MatrixXd Y;
    Y = AA;
    AA.resize(Y.rows() + C.cols(),n);
    AA<<Y,C.transpose();
    Y = BB;
    BB.resize(Y.rows() + C.cols(),1);
    BB<<Y,MatrixXd::Zero(C.cols(),1);
    MatrixXd OO = MatrixXd::Zero(AA.rows(),AA.rows());
    MatrixXd LL(Covariance.rows() + AA.rows(),Covariance.rows() + AA.rows());
    LL<<Covariance,AA.transpose(),AA,OO;
    NN = LL.fullPivHouseholderQr().solve(BB);
  }
  p.w = NN.head(n);
  p.calculateSD();
  return p.sd;
}
